import React from "react";

import Form from '../src';

export default function App() {
  return (
    <Form
      title="Title is here"
      description="Description is here"
      input={{
        // value: "Initial value",
        multiline: true,
        // minRows: 3,
        // copy: true,
        // send: true,   
        onChange: (value) => console.log("New value:", value),
        onEnter: (value) => console.log("Final value:", value),
      }}
    // controls={[
    //     { title: "Send" }
    // ]}
    />
  );
}