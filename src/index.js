import React from "react";
import Layout from "@fnet/react-layout-asya";

import TextField from "@mui/material/TextField";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import SendIcon from "@mui/icons-material/Send";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";

import { CopyToClipboard } from "react-copy-to-clipboard";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default (props) => {
  // This state holds the current value for the TextField
  const [value, setValue] = React.useState(props.input?.value || "");

  // State for controlling the Snackbar open/close
  const [state, setState] = React.useState({
    open: false,
    vertical: "bottom",
    horizontal: "center",
  });

  // Handle changes in the TextField's value
  // Also calls props.input?.onChange if provided
  const handleValueChange = React.useCallback(
    (event) => {
      setValue(event.target.value);
      if (props.input?.onChange) {
        props.input.onChange({ value: event.target.value });
      }
    },
    [props.input]
  );

  // Handle key presses: 
  //   If multiline is true, Ctrl+Enter (or Cmd+Enter) triggers onEnter
  //   If multiline is false, pressing Enter triggers onEnter
  const handleKeyDown = React.useCallback(
    (event) => {
      if (props.input?.multiline) {
        if ((event.ctrlKey || event.metaKey) && event.key === "Enter") {
          event.preventDefault();
          if (props.input?.onEnter) {
            props.input.onEnter(value);
          }
          setValue("");
        }
      } else {
        if (event.key === "Enter") {
          event.preventDefault();
          if (props.input?.onEnter) {
            props.input.onEnter(value);
          }
          setValue("");
        }
      }
    },
    [props.input, value]
  );

  // Handle the event after copying
  const handleCopy = () => {
    setState({ ...state, open: true });
  };

  // Close the Snackbar
  const handleClose = () => {
    setState({ ...state, open: false });
  };

  // Handle the "Send" button click:
  //   Calls onEnter and resets the value to an empty string
  const handleSend = React.useCallback(() => {
    if (props.input?.onEnter) {
      props.input.onEnter(value);
    }
    setValue("");
  }, [props.input, value]);

  // Expose a setValue function to parent components via props.form
  React.useEffect(() => {
    if (props.form) {
      props.form.setValue = (val) => setValue(val);
    }
  }, [props.form]);

  return (
    <Layout {...props}>
      <TextField
        fullWidth
        autoFocus={true}
        disabled={props.input?.disabled || false}
        type={props.input?.type}
        multiline={props.input?.multiline || false}
        rows={props.input?.rows}
        maxRows={props.input?.maxRows || 5}
        minRows={props.input?.minRows || 1}
        required={props.input?.required || false}
        placeholder={props.input?.placeholder}
        variant={props.input?.variant || "standard"}
        value={value}
        onChange={handleValueChange}
        onKeyDown={handleKeyDown}
        sx={{
          width: "100%",
          backgroundColor: "rgba(255,255,255,0.5)",
          borderTopLeftRadius: "4px",
          borderTopRightRadius: "4px",
        }}
        InputProps={{
          readOnly: props.input?.readOnly || false,
          endAdornment: (
            <InputAdornment position="end">
              {/* Render Copy icon if props.input?.copy is true */}
              {props.input?.copy && (
                <>
                  <CopyToClipboard text={value} onCopy={handleCopy}>
                    <IconButton>
                      <ContentCopyIcon />
                    </IconButton>
                  </CopyToClipboard>
                  <Snackbar
                    anchorOrigin={{
                      vertical: state.vertical,
                      horizontal: state.horizontal,
                    }}
                    open={state.open}
                    onClose={handleClose}
                    key={state.vertical + state.horizontal}
                  >
                    <Alert severity="success">Copied to clipboard!</Alert>
                  </Snackbar>
                </>
              )}

              {/* Render Send button if props.input?.send is true */}
              {props.input?.send !== false && (
                <IconButton
                  onClick={handleSend}
                  // Disable the button if there is no text
                  disabled={!value.trim()}
                >
                  <SendIcon />
                </IconButton>
              )}
            </InputAdornment>
          ),
          sx: {
            fontSize: "calc(1.2vh + 1.2vw)",
            paddingLeft: "4px",
            paddingRight: "4px",
            lineHeight: "calc(2vh + 2vw)",
          },
        }}
      />
    </Layout>
  );
};