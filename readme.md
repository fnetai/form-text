# @fnet/form-text

## Introduction

The `@fnet/form-text` is a simple React component designed to provide users with a flexible text input field within a form layout. It integrates essential features such as the ability to copy text to the clipboard and customizable text field properties. The component focuses on ease of use and offers a straightforward solution for handling text input in a React application.

## How It Works

The `@fnet/form-text` component utilizes a `TextField` from Material-UI to create a customizable text input area within a React layout. Users can input text, which can be configured to meet specific requirements like multiline input, read-only mode, and more. The integrated copy-to-clipboard function allows users to easily copy the input text to their clipboard with a simple click, triggering a confirmation snackbar.

## Key Features

- **Customizable Input Field**: Users can set options such as `disabled`, `multiline`, `rows`, `placeholder`, and `variant` to tailor the input field to specific needs.
- **Copy to Clipboard Functionality**: Includes a built-in feature to copy the input text to the clipboard, with a visual confirmation using a snackbar.
- **Responsive Design**: Automatically adjusts text size and padding for a comfortable user experience across different devices.
- **Integrated with React Layouts**: Works seamlessly within existing React layouts to provide a uniform appearance with other form components.

## Conclusion

The `@fnet/form-text` component is a helpful addition for React developers looking for a customizable text input field with integrated copy-to-clipboard functionality. It offers straightforward configuration options and integrates well within form layouts, ultimately enhancing user interactions with minimal effort.